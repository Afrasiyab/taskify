Open Intellij, add dependencies to pom.xml then run application and tests.

Endpoints:

POST:

localhost:8888/api/auth/signup - endpoint for register user

localhost:8888/api/auth/signin - endpoint for user sign in

localhost:8888/api/user - admin create user with default password

localhost:8888/api/tasks/create - user create and assign task to user(s)

GET: 

localhost:8888/api/tasks - user list his/her organization's tasks

localhost:8888/api/test/user - test user exists or not
